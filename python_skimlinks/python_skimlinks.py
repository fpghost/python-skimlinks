"""Skimlinks Reporting API wrapper. By Lee H"""
import logging
import time
import requests
from hashlib import md5


logger = logging.getLogger(__name__)

ENCODING = 'utf-8'
API_ROOT = 'https://reporting.skimapis.com'
MOCK_API_ROOT = 'https://private-anon-9061098063-skimlinksreporting.apiary-mock.com'
MOCK_ACCOUNT_ID = '123'


class InvalidParameter(Exception):
    pass


class Client(object):
    """Skimlinks client."""

    _apis = frozenset(('commission-report', 'reports'))

    def __init__(self, account_id, account_type, private_key, mock=False):
        """Initialise the client."""
        if mock:
            self._api_root = MOCK_API_ROOT
            self._account_id = MOCK_ACCOUNT_ID
        else:
            self._account_id = account_id
            self._api_root = API_ROOT
        self._account_type = account_type
        self._private_key = private_key

    def _timestamp(self):
        return int(time.time())

    def _auth(self, timestamp):
        """
        Hash timestamp and private key
        """
        return md5("{}{}".format(timestamp, self._private_key).encode()).hexdigest().lower()

    @staticmethod
    def _get(url, headers=None, params=None):
        """HTTP GET request."""
        logger.debug(url)
        logger.debug(headers)
        logger.debug(params)
        print(url)
        print(headers)
        print(params)
        response = requests.get(url, headers=headers, params=params)
        response.raise_for_status()
        # If JSON fails, return raw data
        # (e.g. when downloading CSV job logs).
        try:
            return response.json()
        except ValueError:
            return response.text

    @staticmethod
    def _post(url, data, headers=None, params=None):
        """HTTP POST request."""
        response = requests.post(url, params=params, data=data, headers=headers)
        response.raise_for_status()
        return response.json()

    def endpoint(self, name):
        """Generate the URL endpoint for the given API."""
        return '{0}/{1}/{2}/{3}'.format(self._api_root, self._account_type, self._account_id, name)

    def api(self, name, **kwargs):
        """Generic API method."""
        if name not in self._apis:
            raise ValueError('API name must be one of {0}, not {1!r}.'.format(
                tuple(self._apis), name))

        # Make the URL
        url = self.endpoint(name)

        # Headers
        headers = kwargs.pop('headers', {'Accept': 'application/json'})
        if 'accept' not in [k.lower() for k in headers]:
            headers['accept'] = 'application/json'

        # Required query params
        timestamp = self._timestamp()
        params = {'timestamp': timestamp, 'token': self._auth(timestamp)}

        # Opt query params
        params.update(kwargs)

        return self._get(url, headers=headers, params=params)

    def search_commissions(self, **kwargs):
        """Search commissions API."""
        allowed_params = frozenset(('headers', 'limit', 'offset', 'start_date', 'end_date',
                                    'updated_since', 'custom_id', 'merchant_id',
                                    'domain_id', 'status'))
        for kwarg in kwargs:
            if kwarg not in allowed_params:
                msg = '{} is not a valid query param for this endpoint'.format(kwarg)
                logger.warning(msg)
                raise InvalidParameter(msg)

        return self.api('commission-report', **kwargs)

    def aggregated_reports(self, **kwargs):
        """Aggregated reports API."""
        allowed_params = frozenset(('headers', 'report_by', 'start_date', 'end_date',
                                    'limit', 'offset', 'sort_by',
                                    'sort_dir', 'time_period', 'a_id',
                                    'domain_id', 'page_search', 'link_search',
                                    'merchant_search'))
        for kwarg in kwargs:
            if kwarg not in allowed_params:
                msg = '{} is not a valid query param for this endpoint'.format(kwarg)
                logger.warning(msg)
                raise InvalidParameter(msg)
        return self.api('reports', **kwargs)
